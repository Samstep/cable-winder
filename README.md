  Project Title: Activity 3.1.7

  Date: 5/20/19
  Section: Final Project: Cable winding Mechanism


  Task Description: A telecommunications contractor needs your team to design a device that can accurately wind up a specific
  length of cable. The device must be able to wind a specific length consistently. The device must also be able to be started
  and stopped (emergency) by using a switch.

  Pseudocode:
  When start button is pressed motor will wind cable out and stop once it has finished winding out all the to the set amount of inches.
  When emergency stop button is pressed the motor will stop no matter what.
  Reset button will wind in cable mechanism all the way.

  --Defined config--
   mstart - touch sensor, buton --> start the cable winder
   estop - touch sensor, button --> stop the cable winder
   pmotor - 263 motor --> control cable winder
   reset - touch sensor, button --> re-extend cable winder
   encoder - encoder --> count rotations
   green - led --> status LED, is on when winder winds cable down.


